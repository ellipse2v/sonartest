package S2384_SonarLint;

 

import java.util.Arrays;

import java.util.Collections;

import java.util.HashSet;

import java.util.Set;

 
/**
*test a Sonar Rule
**/
public class test_S2384 {
     

       /**

       * enum for test.

       *

       */

       public enum testWithEnum {

            

             NONE,

            

             WITH,

            

             WITHOUT

       }

      

       /**

       *     // Singleton collections

 

   

     * Returns an immutable set containing only the specified object.

     * The returned set is serializable.

     *

     * @param  <T> the class of the objects in the set

     * @param o the sole object to be stored in the returned set.

     * @return an immutable set containing only the specified object.

    

    public static <T> Set<T> singleton(T o) {

        return new SingletonSet<>(o);

    }

       */

       private static final Set<testWithEnum> set = new HashSet<testWithEnum>(Arrays.asList(testWithEnum.WITH));

      

       private static final Set<testWithEnum> setEnum = Collections.singleton(testWithEnum.WITH);

      

       private static final Set<testWithEnum> setEnumTest2 = Collections.unmodifiableSet(new HashSet<testWithEnum>(Arrays.asList(testWithEnum.WITH)));

      

       /**

       * return the set.

       * @return set.

       */

       public Set<testWithEnum> getSet() {

             return set;

       }

      

       /**

       * return the setEnum.

       * @return setEnum.

       */

       public Set<testWithEnum> getSetEnum() {

             return setEnum;

       }

      

       /**

       * return the setEnumTest2.

       * @return setEnumTest2.

       */

       public Set<testWithEnum> getSetEnumTest2() {

             return setEnumTest2;

       }

}